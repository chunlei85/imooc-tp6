<?php
/*
 * @Author: your name
 * @Date: 2020-02-01 22:55:09
 * @LastEditTime: 2020-02-18 23:49:19
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\model\mysql\Test.php
 */

namespace app\common\model\mysql;

use think\Model;

class Category extends Model
{

    // 获取分类列表
    public function getNormalCategorys($field)
    {
        $where = [
            'status' => config('status.mysql.table_normal'),
        ];
        return $this->where($where)->field($field)->select();
    }

    // 获取分页分类列表
    public function getLists($where,$num)
    {
        $order = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        $result = $this->where('status','<>',config('status.mysql.table_delete'))
        ->where($where)->order($order)->paginate($num);
        return $result;
    }

    // 根据ID更新数据
    public function updateById($id,$data)
    {
        $data['update_time'] = time();
        return $this->where(['id' => $id])->save($data);
    }

}