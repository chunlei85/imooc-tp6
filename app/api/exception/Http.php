<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 15:04:08
 * @LastEditTime : 2020-02-12 23:11:58
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\exception\Http.php
 */

namespace app\api\exception;

use think\exception\Handle;
use think\Response;
use Throwable;


class Http extends Handle
{

    private $httpStatus = 500;

    public function render($request, Throwable $e): Response
    {
        if($e instanceof \think\Exception){
            return show($e->getCode(),$e->getMessage());
        }
        if($e instanceof \think\exception\HttpResponseException){
            return parent::render($request,$e);
        }
        if(method_exists($e,'getStatusCode')){
            $this->httpStatus = $e->getStatusCode();
        }
        return show(config('status.error'),$e->getMessage(),[],$this->httpStatus);
    }

}