<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 14:20:30
 * @LastEditTime : 2020-02-05 22:16:05
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\route\demo.php
 */

use think\facade\Route;

Route::rule('smscode','api/Sms/code','POST');
Route::rule('login','api/Login/index','POST');
Route::resource('user','User');
Route::resource('logut','Logut');