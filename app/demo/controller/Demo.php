<?php
/*
 * @Author: your name
 * @Date: 2020-02-01 15:13:46
 * @LastEditTime : 2020-02-02 14:43:53
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\controller\Demo.php
 */

namespace app\demo\controller;

use app\BaseController;

class Demo extends BaseController
{

    public function show()
    {
        
    }

    public function hello()
    {
        echo time();
    }

}