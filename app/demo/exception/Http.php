<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 15:04:08
 * @LastEditTime : 2020-02-02 15:15:52
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\exception\Http.php
 */

namespace app\demo\exception;

use think\exception\Handle;
use think\Response;
use Throwable;


class Http extends Handle
{

    private $httpStatus = 500;

    public function render($request, Throwable $e): Response
    {
        if(method_exists($e,'getStatusCode')){
            $this->httpStatus = $e->getStatusCode();
        }
        return show(config('status.error'),$e->getMessage(),[],$this->httpStatus);
    }

}