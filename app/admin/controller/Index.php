<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 21:58:59
 * @LastEditTime : 2020-02-04 22:33:15
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\controller\Index.php
 */

namespace app\admin\controller;

use think\facade\View;

class Index extends AdminBase
{

    public function index()
    {
        return View::fetch();
    }

    public function welcome()
    {
        return View::fetch();
    }

}
