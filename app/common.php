<?php
/*
 * @Author: your name
 * @Date: 2020-01-31 22:56:23
 * @LastEditTime : 2020-02-02 22:02:32
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common.php
 */
// 应用公共文件

function show($status,$message='error',$data=[],$httpStatus = 200)
{

    $result = [
        'status' => $status,
        'message' => $message,
        'result' => $data
    ];

    return json($result,$httpStatus);
    
}