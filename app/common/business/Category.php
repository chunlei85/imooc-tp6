<?php
/*
 * @Author: your name
 * @Date: 2020-02-04 23:57:41
 * @LastEditTime: 2020-02-19 00:16:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\business\AdminUser.php
 */

namespace app\common\business;

use app\common\model\mysql\Category as CategoryModel;
use Exception;

class Category{

    protected $model = null;

    public function __construct()
    {
        $this->model = new CategoryModel();
    }

    public function add($data)
    {
        $data['status'] = config('status.mysql.table_normal');
        $name = $data['name'];
        $result = $this->model->where(['name' => $name])->find();
        if($result){
            throw new Exception('已存在分类');
        }
        try{
            $this->model->save($data);
        }catch(Exception $e){
            throw new Exception('服务器内部错误');
        }
        return $this->model->getLastInsID();
    }

    // 根据条件获取分类列表
    public function getLists($data,$num)
    {
        $categorys = $this->model->getLists($data,$num);
        if(!$categorys){
            return [];
        }
        return $categorys->toArray();
    }

    // 获取分类列表
    public function getNormalCategorys()
    {
        $field = 'id,name,pid';
        $categorys = $this->model->getNormalCategorys($field);
        if(!$categorys){
            return [];
        }
        return $categorys->toArray();
    }

    // 排序
    public function listorder($id,$listorder)
    {
        $result = $this->getById($id);
        if(!$result){
            throw new Exception('不存在该记录');
        }
        $data = [
            'listorder' => $listorder,
        ];
        try{
            $result = $this->model->updateById($id,$data);
        }catch(Exception $e){
            return false;
        }
        return $result;
    }

    // 根据ID获取数据
    public function getById($id)
    {
        $result = $this->model->find($id);
        if(empty($result)){
            return [];
        }
        return $result->toArray();
    }

    // 更新状态
    public function status($id,$status)
    {
        $result = $this->getById($id);
        if(!$result){
            throw new Exception('不存在该记录');
        }
        if($result['status'] == $status){
            throw new Exception('状态修改前和修改后一样没有任何意义哦');
        }
        $data = [
            'status' => intval($status),
        ];
        try{
            $result = $this->model->updateById($id,$data);
        }catch(Exception $e){
            return false;
        }
        return $result;
    }

}
