<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 21:58:59
 * @LastEditTime: 2020-02-19 00:14:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\controller\Index.php
 */

namespace app\admin\controller;

use app\admin\validate\CategoryValidate;
use app\common\business\Category as CategoryBus;
use app\common\lib\Status as StatusLib;
use Exception;
use think\facade\View;

class Category extends AdminBase
{

    public function index()
    {
        $pid = input('param.pid',0,'intval');
        $data = [
            'pid' => $pid,
        ];
        try{
            $categorys = (new CategoryBus())->getLists($data,5);
        }catch(Exception $e){
            $categorys = [];
        }
        return View::fetch('',[
            'categorys' => $categorys,
        ]);
    }

    // 添加分类
    public function add()
    {
        try{
            $categorys = (new CategoryBus())->getNormalCategorys();
        }catch(Exception $e){
            $categorys = [];
        }
        return View::fetch('',[
            'categorys' => json_encode($categorys),
        ]);
    }

    // 保存分类
    public function save()
    {
        $pid = input('param.pid',0,'intval');
        $name = input('param.name','','trim');

        $data = [
            'pid' => $pid,
            'name' => $name,
        ];
        $validate = new CategoryValidate();
        if(!$validate->check($data)){
            return show(config('status.error'), $validate->getError());
        }

        try{
            $result = (new CategoryBus())->add($data);
        }catch(Exception $e){
            return show(config('status.error'), $e->getMessage());
        }
        if($result){
            return show(config('status.success'), 'OK');
        }else{
            return show(config('status.error'), 'ERROR');
        }
    }

    // 排序
    public function listorder()
    {
        $id = input('param.id',0,'intval');
        $listorder = input('param.listorder',0,'intval');
        $data = [
            'id' => $id,
            'listorder' => $listorder,
        ];
        $validate = new CategoryValidate();
        if(!$validate->scene('listorder')->check($data)){
            return show(config('status.error'), $validate->getError());
        }
        try{
            $result = (new CategoryBus())->listorder($id,$listorder);
        }catch(Exception $e){
            return show(config('status.error'), $e->getMessage());
        }
        if($result){
            return show(config('status.success'), '排序成功');
        }else{
            return show(config('status.error'), '排序错误');
        }
    }

    // 更新状态
    public function status()
    {
        $id = input('param.id',0,'intval');
        $status = input('param.status',0,'intval');
        $data = [
            'id' => $id,
            'status' => $status
        ];
        $validate = new CategoryValidate();
        if(!$validate->scene('status')->check($data)){
            return show(config('status.error'), $validate->getError());
        }
        if(!in_array($status,StatusLib::getTableStatus())){
            return show(config('status.error'), '状态错误');
        }
        try{
            $result = (new CategoryBus())->status($id,$status);
        }catch(Exception $e){
            return show(config('status.error'), $e->getMessage());
        }
        if($result){
            return show(config('status.success'), '更新成功');
        }else{
            return show(config('status.error'), '更新错误');
        }
        
    }
    
}
