<?php
/*
 * @Author: your name
 * @Date: 2020-02-01 21:57:17
 * @LastEditTime : 2020-02-01 22:20:37
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\controller\Error.php
 */

namespace app\demo\controller;

class Error{

    // 调用不存在的方法
    public function __call($name, $arguments)
    {
        return show(config('status.controller_not_found'),"找不到该控制器",null,400);
    }

}