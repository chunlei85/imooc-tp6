<?php
/*
 * @Author: your name
 * @Date: 2020-02-05 22:08:03
 * @LastEditTime : 2020-02-13 01:05:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\controller\Sms.php
 */

namespace app\api\controller;

use app\BaseController;
use app\common\business\Sms as SmsBus;
use think\exception\ValidateException;
use think\Log;

class Sms extends BaseController{

    public function code()
    {
        // 随机数做流量控制
        $phoneNumber = input('param.phone_number','','trim');
        $data = [
            'phone_number' => $phoneNumber,
        ];
        try{
            validate(\app\api\validate\UserValidate::class)->scene('send_code')->check($data);
        }catch(ValidateException $e){
            return show(config('status.error'),$e->getMessage());
        }
        // 调用
        if(SmsBus::sendCode($phoneNumber,6)){
            return show(config('status.success'),'验证码发送成功');
        }
        return show(config('status.error'),'验证码发送失败');

    }

}