<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 21:34:55
 * @LastEditTime : 2020-02-02 21:39:34
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edits
 * @FilePath: \imooc-tp6\app\admin\controller\Verify.php
 */

namespace app\admin\controller;

use app\BaseController;
use think\captcha\facade\Captcha;

class Verify extends BaseController
{
    
    public function index()
    {
        return Captcha::create('abc');
    }

}