<?php
/*
 * @Author: your name
 * @Date: 2020-02-16 18:13:39
 * @LastEditTime: 2020-02-16 18:14:05
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\controller\Login.php
 */

namespace app\api\controller;

use app\api\validate\UserValidate;
use app\BaseController;

class Login extends BaseController{

    public function index()
    {
        if(!$this->request->isPost()){
            return show(config('status.error'), '非法请求');
        }
        $phoneNumber = $this->request->param('phone_number','','trim');
        $code = $this->request->param('code',0,'intval');
        $type = $this->request->param('type',0,'intval');
        $data = [
            'phone_number' => $phoneNumber,
            'code' => $code,
            'type' => $type,
        ];
        $validate = new UserValidate();
        if(!$validate->scene('login')->check($data)){
            return show(config('status.error'), $validate->getError());
        }
        try{
            $result = (new \app\common\business\User())->login($data);
        }catch(\Exception $e){
            return show($e->getCode(),$e->getMessage());
        }
        if($result){
            return show(config('status.success'),'登录成功',$result);
        }
        return show(config('status.error'),'登录失败');

    }


}