<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 18:36:31
 * @LastEditTime : 2020-02-04 23:28:16
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\middleware\check.php
 */

namespace app\admin\middleware;

class Auth
{

    public function handle($request,\Closure $next)
    {


        // 前置中间件
        if(empty(session(config('admin.session_admin'))) && !preg_match('/login/',$request->pathinfo())){
            // 判断是否是ajax请求
            return redirect(url('login/index'));
        }

        $response = $next($request);
        
        // 后置中间件
        // if(empty(session(config('admin.session_admin'))) && $request->controller() != 'Login'){
        //     // 判断是否是ajax请求
        //     return redirect(url('login/index'));
        // }

        return $response;
    }

}
