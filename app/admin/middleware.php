<?php
/*
 * @Author: your name
 * @Date: 2020-02-03 21:39:39
 * @LastEditTime : 2020-02-04 23:09:38
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\middleware.php
 */
// 全局中间件定义文件
return [
    // 全局请求缓存
    // \think\middleware\CheckRequestCache::class,
    // 多语言加载
    // \think\middleware\LoadLangPack::class,
    // Session初始化
    \think\middleware\SessionInit::class,
    app\admin\middleware\Auth::class,
];
