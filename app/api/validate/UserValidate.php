<?php
/*
 * @Author: your name
 * @Date: 2020-02-05 22:39:42
 * @LastEditTime : 2020-02-05 22:55:04
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\validate\UserValidate.php
 */

namespace app\api\validate;

use think\Validate;

class UserValidate extends Validate{

    protected $rule = [
        'username' => 'require',
        'code' => 'require|number|min:4',
        'type' => 'require|in:1,2',
        'phone_number' => 'require',
        'sex' => 'require|in:0,1,2'
    ];

    protected $message = [
        'username.require' => '用户名不能为空',
        'phone_number.require' => '手机号不能为空',
        'code.require' => '短信验证码必须',
        'code.number' => '短信验证码必须为数字',
        'code.min' => '短信验证码长度不得低于4',
        'type.require' => '类型必须',
        'type.in' => '类型数据错误',
        'sex.require' => '性别不能为空',
        'sex.in' => '性别数据错误',
    ];

    protected $scene = [
        'send_code' => ['phone_number'],
        'login' => ['phone_number','code','type'],
        'update_user' => ['username','sex'],
    ];

}