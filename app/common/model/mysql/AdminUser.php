<?php
/*
 * @Author: your name
 * @Date: 2020-02-03 22:12:21
 * @LastEditTime : 2020-02-04 21:22:57
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\model\mysql\AdminUser.php
 */

namespace app\common\model\mysql;

use think\Model;

class AdminUser extends Model
{

    public function getAdminUserByUsername($username)
    {
        if(empty($username)){
            return [];
        }
        
        $where = [
            'username' => $username,
        ];

        $result = $this->where($where)->find();
        return $result;
    }


    // 根据主键更新数据
    public function updateById($id,$data)
    {
        $id = intval($id);
        if(empty($id) || empty($data) || !is_array($data)){
            return [];
        }
        $where = [
            'id' => $id,
        ];
        $result = $this->where($where)->update($data);
        return $result;
    }
    
}
