<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 14:20:30
 * @LastEditTime : 2020-02-02 18:40:49
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\route\demo.php
 */

use think\facade\Route;

Route::rule('test','demo/hello','GET')->middleware(app\demo\middleware\Check::class);